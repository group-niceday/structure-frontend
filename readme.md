# 프론트엔드 개발 기준(structure-frontend)
  * frontend 개발을 위한 기본 개발환경을 제공한다.  

# 이력
  * v0.0.1          
    * 2020.02.03 
      * 최초 등록

# 구성
  <!-- blank line -->
  * 환경
    * windows 10, mac
    * webstorm 2019.3.2
    * node v12.14.2
    * npm v6.13.4
    * vue-cli v4.1.2

  <!-- blank line -->
  * 라이브러리
    * vue-class-component
      * https://github.com/vuejs/vue-class-component
    * class-validator
      * https://github.com/typestack/class-validator
    * class-transformer
      * https://github.com/typestack/class-transformer
    * vuex-smart-module
      * https://github.com/ktsn/vuex-smart-module
    * vue-property-decorator
      * https://github.com/kaorun343/vue-property-decorator
    * axiosService
      * https://github.com/axiosService/axiosService
    * lodash
      * https://lodash.com
    * qs
      * https://github.com/ljharb/qs
     

# 설치 및 실행
<!-- blank line -->
  * 설치
    * Node 모듈 설치 (<a href="https://nodejs.org/ko/">Link</a>)
    * Git 설치 (<a href="https://git-scm.com/">Link</a>)
    * vue-cli 설치 (<a href="https://cli.vuejs.org/">Link</a>)
    * Webstrom 설치 (<a href="https://www.jetbrains.com/ko-kr/webstorm/download/#section=windows">Link</a>)
  
  * 실행
    * npm install
    * npm run serve
    * npm run build

  ## WebStorm
  * CodeStyle 설정
    * `setting/webstorm-code-style.xml` 파일을 설정의 `Preferences > Editor > Code Style`에서 `Import Scheme` 한다.
  * Key Mapping 셋팅
    * "Reformat Code" 항목 단축키 등록
  
# 개발
<!-- blank line -->
  ## 패키지 및 파일 명명 규칙
  * 모든 화면 파일명(*.vue)은 "kebab-case"로 작성한다. 
    * Good: sample-list.vue
    * Bad: SampleList.vue
  * 모든 화면 파일명(*.vue)은 기능의 해당하는 두가지 단어 이상으로 작성한다. 
    * Good: sample-list.vue
    * Bad: sample.vue
  * 모든 기능 파일명(*.ts)은 "kebab-case" + "dot.case"로 작성한다.
    * Good: platform.service.ts
    * Bad: platform-service.ts
    * 기능 ex) service, model, router, service...
  * 모든 폴더는 "kebab-case"로 작성한다.
    * Good: platform, tv-platform
    * Bad: TvPlatform, tvPlatform
  * 기타 파일은 "kebab-case"로 작성한다.
    * Good: style.css, default-style.css
    * Bad: defaultStyle.css
  
  ## 패키지 구조
  ```
  ├── public
  │   ├── favicon.icon
  │   └── index.html
  ├── node_modules
  ├── src
  │   ├── app
  │   │   ├── {{대분류}}
  │   │   │   └── {{중분류}}
  │   │   │       └── {{소분류}}
  │   │   │           ├── components
  │   │   │           ├── config
  │   │   │           ├── model
  │   │   │           ├── enum
  │   │   │           ├── router
  │   │   │           ├── service
  │   │   │           ├── view
  │   │   │           └── vuex
  │   │   ├── auth
  │   │   ├── guide
  │   │   ├── system
  │   │   └── shared
  │   │       ├── components
  │   │       ├── config
  │   │       ├── directive
  │   │       ├── enum
  │   │       ├── filter
  │   │       ├── model
  │   │       ├── router
  │   │       ├── service
  │   │       └── vuex
  │   ├── assets
  │   │   ├── css
  │   │   ├── font
  │   │   └── images
  │   ├── modules
  │   │   └── @types
  │   │       └── @{{명칭}}
  │   ├── core
  │   │   ├── decorator
  │   │   └── service
  │   ├── app.vue
  │   ├── components.ts
  │   ├── main.ts
  │   ├── router.ts
  │   └── shims-vue.d.ts
  ├── .gitignore
  ├── babel.config.js
  ├── env.development
  ├── env.product
  ├── package.json
  ├── postcss.config.js
  ├── readme.md
  ├── tsconfig.json
  ├── tslint.json
  └── vue.config.js
  ```
  ## 상위 패키지 구조 설명   
  | Package                               | Description |
  | --------                              |  -------- |
  | /node_modules                         | NPM 라이브러리 영역 |
  | /public                               | 최상위 html 영역 |
  | /src/app                              | 화면 주요구성요소 구현영역 |
  | /src/app/shared                       | 화면 주요구성요서 공통영역 구현영역 |
  | /assets                               | 화면 구성을 보조하는 영역 |
  | /core                                 | 프로젝트 기본 구성요소 영역 |
  | /modules                              | 프로젝트 추가 라이브러리 보조영역 |
  | /publish                              | 퍼블리싱 구성영역 |
    
  ## 패키지 구조 설명   
  | Package                               | Description |
  | --------                              |  -------- |
  | /src/app/화면구성영역                    | 화면 영역 |
  | /src/app/auth                         | 인증 구성 영역 |
  | /src/app/guide                        | 셈플페이지 구성 영역 |
  | /src/app/system                       | 화면기본 구성 영역 |
  | /src/app/shared/components            | 화면 공통 component 구성 영역 |
  | /src/app/shared/config                | 화면 공통 config 구성 영역 |
  | /src/app/shared/directive             | 화면 공통 directive 구성 영역 |
  | /src/app/shared/enum                  | 화면 공통 enum 구성 영역 |
  | /src/app/shared/filter                | 화면 공통 filter 구성 영역 |
  | /src/app/shared/model                 | 화면 공통 model 구성 영역 |
  | /src/app/shared/router                | 화면 공통 router 구성 영역 |
  | /src/app/shared/service               | 화면 공통 service 구성 영역 |
  | /src/app/shared/vuex                  | 화면 공통 vuex 구성 영역 |
  | /src/assets/css                       | 전체 css 구성 영역 |
  | /src/assets/font                      | 전체 font 구성 영역 |
  | /src/assets/images                    | 전체 images 구성 영역 |
  | /src/modules/@types                   | 라이브러리 typescript index 구성 영역 |
  | /src/core/decorator                   | 전역 decorator 구성 영역 |
  | /src/core/service                     | 전역 service 구성 영역 |

  ## 화면 별 패키지 구조
  ```
  ├── components
  ├── config
  ├── model
  ├── enum
  ├── router
  ├── service
  ├── view
  └── vuex
      ├── action
      ├── getter
      ├── mutatuion
      ├── state
      └── store
  ```
 
  ## 화면 별 패키지 구조 설명
  | Package        | Description |
  |  --------      |  -------- |
  | /components    | 컴포넌트 영역 |
  | /config        | 설정 영역 |
  | /model         | 모델 영역 |
  | /enum          | Enum 영역 |
  | /router        | 라우터 영역 |
  | /service       | 서비스 영역 |
  | /view          | 화면 영역 |
  | /vuex/action   | vuex action 영역 |
  | /vuex/getter   | vuex getter 영역 |
  | /vuex/mutation | vuex mutation 영역 |
  | /vuex/state    | vuex state 영역 |
  | /vuex/store    | vuex store 영역 |
  
# 코딩 명명 규칙
  ## 함수명
  | 함수명          | 설명 |
  |  --------      |  -------- |
  | onAdd          | 등록 페이지 이동 |
  | onModify       | 수정 페이지 이동 |
  | onCancel       | 이전 페이지 이동 |
  | onRow          | 목록 테이블 ROW 데이터 클릭 |
  | onSave         | 등록 FORM Action |
  | onUpdate       | 수정 FORM Action |
  | onDelete       | 삭제 FORM Action |
  | onSearch       | 검색 FORM Action |
  
  ## Action
  | Action 명       | 설명 |
  |  --------       |  -------- |
  | getAll          | 목록 조회 |
  | getOne          | 상세 조회 |
  | getPage         | 목록 페이지 조회 |
  | add             | 등록 |
  | update          | 수정 |
  | delete          | 삭제 |
    
  ## 접근제어자
  * 모든 public 접근제어자는 생략한다.
  * 화면에서 사용하는 변수, 함수는 public 이다.
  * class 내부적으로 사용하는 함수, 변수는 private를 명시적으로 선언한다.
  
  ## 명명규칙
  | 단위            | 설명 |
  |  --------      |  -------- |
  | variable       | camelCase |
  | function       | camelCase |
  | class          | PascalCase |
  | folder         | kebab-case |
  | *.ts           | kebab-case + dot.case + .ts |
  | *.css          | kebab-case + .* |
  | *.vue          | kebab-case + .vue |
  | *.other        | kebab-case + .* |

# 참조
  ## 패키지 구조
  * Vue webpack package
    * http://vuejs-templates.github.io/webpack/
  * Vue.js 2 + Vuex + Router + yarn!
    * https://medium.com/tldr-tech/vue-js-2-vuex-router-yarn-basic-configuration-version-2-7b9c489d43b3
  * How to Structure a Vue.js Project
    * https://itnext.io/how-to-structure-a-vue-js-project-29e4ddc1aeeb
  * vuejs-component-style-guide
    * https://github.com/pablohpsilva/vuejs-component-style-guide
    
