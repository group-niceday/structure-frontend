const webpack = require('webpack');
const CompressionWebpackPlugin = require('compression-webpack-plugin');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
  transpileDependencies: [/node_modules[/\\\\]vue-sticky-directive[/\\\\]/],
  publicPath:            '/',

  configureWebpack: (config) => {
    config.entry = ['babel-polyfill', './src/main.ts'];

    if (process.env.VUE_CLI_MODERN_MODE) {
      config.optimization = {
        runtimeChunk: 'single',
        splitChunks:  {
          chunks:             'all',
          maxInitialRequests: Infinity,
          minSize:            120000,
          maxSize:            200000,
          cacheGroups:        {
            vendor: {
              test: /[\\/]node_modules[\\/]/,
              name(module) {
                const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
                return `npm.${packageName.replace('@', '')}`;
              }
            }
          }
        }
      }

      config.output.filename = '[name].[hash:16].js';

      // config.plugins.push(new BundleAnalyzerPlugin({
      //   analyzerHost: 'local.genie.co.kr',
      //   analyzerPort: 8889,
      //   openAnalyzer: true,
      //   analyzerMode: 'static'
      // }));

      config.plugins.push(new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/));
    }
  },

  chainWebpack: (config) => {
    if (process.env.VUE_CLI_MODERN_MODE) {
      config.optimization.minimize(true);
      config.optimization.splitChunks = {
        name:   'vendor',
        chunks: 'initial'
      };

      config.plugins.delete('prefetch');
      config.plugin('CompressionPlugin').use(CompressionWebpackPlugin);
    }

    if (process.env.NODE_ENV === 'local') {
      config.plugin('fork-ts-checker').tap(args => {
        args[0].memoryLimit = 1024 * 8;
        args[0].workers = 4;
        return args
      });
    }
  },

  productionSourceMap: false,
  devServer:           {
    historyApiFallback: true,
    compress:           true,
    publicPath:         '/',
    host:               '0.0.0.0',
    port:               9093,
    public:             'local.niceday.io:9093'
  }
}
