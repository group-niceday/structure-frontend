export const CODE_EDITOR_CONTENT =
  `<h3 class="list-title">[Ticket Information]</h3>
<p class="list-desc">LEE HI 5th OFFICIAL FANCLUB HICECREAM FAN-CONCERT 24℃ ON VIBE Enjoy watching it live! The live stream starts at 5 PM (KST), on Saturday, 6th of March. You can watch the concert again at 10 AM (KST) on Sunday, 7th of March.</p>
<p class="list-desc">The online event will be an internet live-streaming event. Actual starting and ending times my change depending on on-site and network circumstances.</p>
<p class="list-guide">(*Tis live streaming product does not include a VOD service.)</p>
<h3 class="list-title">[Purchasing Your Ticket]</h3>
<p class="list-depth1">HD Multi-view 54,500won (VAT included)</p>
<ul class="list-depth2">
  <li>HD Multi-view includes one main screen and three concept screens (four total views)</li>
  <li>asd Multi-view is only available for LEE HI GLOBAL OFFICIAL FANCLUB MEMBERSHIP holders. (Membership may be purchased on stayG Shop)</li>
</ul>
<p class="list-depth1">HD Single-view 44,500won (VAT included)</p>
<ul class="list-depth2">
  <li>HD asd-view includes one main screen and three concept screens (four total views)</li>
  <li>HD Multi-view is only available for LEE HI GLOBAL OFFICIAL FANCLUB MEMBERSHIP holders. (Membership may be purchased on stayG Shop)</li>
</ul>
<p class="list-depth1">HD Single-view 44,500won (VAT included)</p>
<ul class="list-depth2">
  <li>HD Multi-view includes one main screen and three concept screens (four total views)</li>
  <li>HD Multi-view is only available for LEE HI GLOBAL OFFICIAL FANCLUB MEMBERSHIP holders. (Membership may be purchased on stayG Shop)</li>
</ul>
<p class="list-depth1">HD Single-view 44,500won (VAT included)</p>
<ul class="list-depth2">
  <li>HD Multi-view includes one main screen and three concept screens (four total views)</li>
  <li>HD Multi-view is only available for LEE HI GLOBAL OFFICIAL FANCLUB MEMBERSHIP holders. (Membership may be purchased on stayG Shop)</li>
</ul>`;

export const CODE_EDITOR_CONTENT_KR =
  `<h3 class="list-title">[티켓 정보]</h3>
<p class="list-desc">LEE HI 5th OFFICIAL FANCLUB HICECREAM FAN-CONCERT 24℃ ON VIBE Enjoy watching it live! The live stream starts at 5 PM (KST), on Saturday, 6th of March. You can watch the concert again at 10 AM (KST) on Sunday, 7th of March.</p>
<p class="list-desc">The online event will be an internet live-streaming event. Actual starting and ending times my change depending on on-site and network circumstances.</p>
<p class="list-guide">(*Tis live streaming product does not include a VOD service.)</p>
`;

export const CODE_EDITOR_CONTENT_PAYMENT =
  `<h2>판매기간</h2>
<span>2021.11.12 12:00 PM ~ 2021.11.12 12:00 PM (KST)</span>

<h2>구매 전 꼭 확인해주세요.</h2>
<ul>
  <li>· 1인 1회만 구매가능합니다.</li>
  <li>· 티켓 구매 후 발급받은 티켓 코드를 등록해야 관람이 가능합니다.</li>
  <li>· 본 공연 시작 1시간 전 결제취소/ 환불이 마감되며. 마감이후에는<br>영상시청 여부와 관계없이 환불이 불가합니다.</li>
  <li>· 본 상품은 다시보기 및 VOD 다운로드를 지원하지 않습니다.</li>
  <li>· 본 영상은 저작권법의 보호를 받는 창작물로, 무단 유포 및 불법 게재<br>할 경우 손해배상 청구를 비롯한 법정 책임을 지게 됩니다.</li>
</ul>

<h2>이용 안내</h2>
<ul>
  <li>· PC와 모바일 웹에서 감상할 수 있습니다.</li>
  <li>· iOS는 세로화면에서만 채팅 및 설문에 참여할 수 있습니다.<br>가로화면(전체화면)에는 제공되지 않습니다.</li>
  <li>· PC 웹 브라우저 버전이 Chrome 35이상, Edge 12이상, Safari 10 이상 (Window, MacOS 지원, 그래픽 카드와 모니터 HDCP 1.4 이상 지원)</li>
</ul>

<h2>환불 안내</h2>
<p>STAYG는 다음의 경우 환불이 불가하오니 구매 전 확인 부탁드립니다.</p>
<ul>
  <li>· 상품을 구매한지 7일로 (청약철회기간)이 지난 경우</li>
  <li>· 상품을 구매한지 7일로 (청약철회기간)이 지나진 않았지만 구매한 상품에 포함된 모든 영상을 한번이라도 이미 재생한 경우</li>
</ul>
`;
