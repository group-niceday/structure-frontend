import 'reflect-metadata';
import {Expose, Type}                                                                                                                                      from 'class-transformer';
import {ArrayNotEmpty, IsArray, IsBoolean, IsHexColor, IsInt, IsNotEmpty, IsNumber, IsOptional, IsString, Max, MaxLength, Min, ValidateIf, ValidateNested} from 'class-validator';

import {Description}                  from '@/core/decorator/description.decorator';
import {IsAbsoluteUrl, IsRelativeUrl} from '@/core/decorator/validator/is-url.decorator';
import {IsI18n}                       from '@/core/decorator/validator/is-i18n.decorator';
import {GuideEnum}                    from '@/app/guide/model/guide.enum';
import {Pageable}                     from '@/app/shared/model/pageable.model';
import {I18n}                         from '@/app/shared/model/i18n.model';
import {I18nFile}                     from '@/app/shared/model/i18n-file.model';
import {SampleAlbum}                  from '@/app/sample/album/model/sample-album.model';
import {FULL_REQUIRE_LANGUAGES}       from '@/app/shared/config/i18n.config';

export namespace Guide {
  export class Search extends Pageable.Request.Search {
    @Expose() @Description('Search1')
    search1!: string;

    @Expose() @Description('Search2')
    search2!: string;
  }

  export class Page {
    @Expose()
    id!: number;

    @Expose()
    name!: string;

    @Expose()
    url!: string;

    @Expose()
    displayYn: boolean = true;

    @Expose()
    createdBy: string = 'pollra';

    @Expose()
    createdAt: string = '2020-04-24T16:05:19.673';

    constructor(index: number, text: string) {
      this.id = index;
      this.name = `${text}-${index}`;
      this.url = `${text}-${index}`;
    }
  }

  export class Form {
    @Expose() @Description('Input Text')
    @IsString() @IsOptional() @MaxLength(200)
    inputText!: string;

    @Expose() @Description('Input Url')
    @ValidateIf(model => !!model.inputUrl)
    @IsString() @IsAbsoluteUrl() @IsOptional() @MaxLength(200)
    inputUrl!: string;

    @Expose() @Description('Input Relative Url')
    @IsString() @IsRelativeUrl() @IsNotEmpty() @MaxLength(200)
    inputRelativeUrl!: string;

    @Expose() @Description('Input Number')
    @IsInt() @IsNotEmpty() @Min(0) @Max(600)
    inputNumber!: number;

    @Expose() @Description('Input Number Option')
    @IsInt() @IsOptional() @Min(0) @Max(600)
    inputNumberOption!: number;

    @Expose() @Description('Input Won')
    @IsInt() @IsNotEmpty() @Min(0)
    inputWon!: number;

    @Expose() @Description('Input Dollar')
    @IsNumber({allowInfinity: false, maxDecimalPlaces: 2}, {message: '숫자를 정확히 입력해 주세요(소수점 2자리 이하로 작성 불가).'})
    @IsNotEmpty() @Min(0)
    inputDollar!: number;

    @Expose() @Description('Input Color')
    @IsString() @IsNotEmpty() @MaxLength(7) @IsHexColor()
    inputColor!: string;

    @Expose() @Description('Select Enum')
    @IsString() @IsNotEmpty()
    selectEnum!: GuideEnum.SELECT.ENUM;

    @Expose() @Description('Select Boolean')
    @IsBoolean() @IsNotEmpty()
    selectBoolean!: boolean;

    @Expose() @Description('Time Picker')
    @IsString() @IsNotEmpty()
    time!: string;

    @Expose() @Description('Date Time Picker')
    @IsString() @IsNotEmpty()
    dateTime!: string;

    @Expose() @Description('Date Time Picker Range Start')
    @IsString() @IsNotEmpty()
    dateTimeRangeStart!: string;

    @Expose() @Description('Date Time Picker Range End')
    @IsString() @IsNotEmpty()
    dateTimeRangeEnd!: string;

    @Expose() @Description('Date Time Second Picker')
    @IsString() @IsNotEmpty()
    dateTimeSecond!: string;

    @Expose() @Description('Date Time Second Picker Range Start')
    @IsString() @IsNotEmpty()
    dateTimeSecondRangeStart!: string;

    @Expose() @Description('Date Time Second Picker Range End')
    @IsString() @IsNotEmpty()
    dateTimeSecondRangeEnd!: string;

    @Expose() @Description('Text Area')
    @IsString() @IsNotEmpty() @MaxLength(200)
    textarea!: string;

    @Expose() @Description('input Text Disable')
    @IsString() @IsNotEmpty() @MaxLength(200)
    inputTextDisable: string = 'input Text Disable';

    @Expose() @Description('Select Enum Disable')
    @IsString() @IsNotEmpty()
    selectEnumDisable: GuideEnum.SELECT.ENUM = GuideEnum.SELECT.ENUM.TEST_1;

    @Expose() @Description('Radio')
    @IsString() @IsNotEmpty()
    radio!: string;

    @Expose() @Description('Select')
    @IsString() @IsNotEmpty()
    select!: string;
  }

  export class GuideI18n {
    @Expose() @Description('require')
    @Type(() => I18n.Request.Default)
    @IsArray() @ArrayNotEmpty() @IsNotEmpty() @ValidateNested() @IsI18n()
    require: I18n.Request.Default[] = [];

    @Expose() @Description('fullRequire')
    @Type(() => I18n.Request.Default)
    @IsArray() @ArrayNotEmpty() @IsNotEmpty() @ValidateNested() @IsI18n(FULL_REQUIRE_LANGUAGES)
    fullRequire: I18n.Request.Default[] = [];

    @Expose() @Description('optional')
    @Type(() => I18n.Request.Default)
    @IsArray() @IsNotEmpty() @ValidateNested()
    optional: I18n.Request.Default[] = [];

    @Expose() @Description('areaRequire')
    @Type(() => I18n.Request.Default)
    @IsArray() @ArrayNotEmpty() @IsNotEmpty() @ValidateNested()
    areaRequire: I18n.Request.Default[] = [];

    @Expose() @Description('areaFullRequire')
    @Type(() => I18n.Request.Default)
    @IsArray() @ArrayNotEmpty() @IsNotEmpty() @ValidateNested()
    areaFullRequire: I18n.Request.Default[] = [];

    @Expose() @Description('areaOptional')
    @Type(() => I18n.Request.Default)
    @IsArray() @IsNotEmpty() @ValidateNested()
    areaOptional: I18n.Request.Default[] = [];

    @Expose() @Description('imageRequire')
    @Type(() => I18nFile.Request.DefaultImage)
    @IsArray() @ArrayNotEmpty() @IsNotEmpty() @ValidateNested()
    imageRequire: I18nFile.Request.DefaultImage[] = [];

    @Expose() @Description('imageFullRequire')
    @Type(() => I18nFile.Request.DefaultImage)
    @IsArray() @ArrayNotEmpty() @IsNotEmpty() @ValidateNested()
    imageFullRequire: I18nFile.Request.DefaultImage[] = [];

    @Expose() @Description('imageOptional')
    @Type(() => I18nFile.Request.DefaultImage)
    @IsArray() @IsNotEmpty() @ValidateNested()
    imageOptional: I18nFile.Request.DefaultImage[] = [];
  }

  export class Connection {
    @Expose() @Description('input1')
    @Type(() => SampleAlbum.Request.Relative)
    @IsNotEmpty() @ValidateNested()
    input1!: SampleAlbum.Request.Relative;
  }

  export class Editor {
    @Expose() @Description('require')
    @Type(() => I18n.Request.Default)
    @IsArray() @ArrayNotEmpty() @IsNotEmpty() @ValidateNested()
    require: I18n.Request.Default[] = [];

    @Expose() @Description('fullRequire')
    @Type(() => I18n.Request.Default)
    @IsArray() @ArrayNotEmpty() @IsNotEmpty() @ValidateNested()
    fullRequire: I18n.Request.Default[] = [];

    @Expose() @Description('optional')
    @Type(() => I18n.Request.Default)
    @IsArray() @IsNotEmpty() @ValidateNested()
    optional: I18n.Request.Default[] = [];

    @Expose() @Description('require max')
    @Type(() => I18n.Request.DefaultMax)
    @IsArray() @ArrayNotEmpty() @IsNotEmpty() @ValidateNested()
    requireMax: I18n.Request.DefaultMax[] = [];

    @Expose() @Description('fullRequire max')
    @Type(() => I18n.Request.DefaultMax)
    @IsArray() @ArrayNotEmpty() @IsNotEmpty() @ValidateNested()
    fullRequireMax: I18n.Request.DefaultMax[] = [];

    @Expose() @Description('optional max')
    @Type(() => I18n.Request.DefaultMax)
    @IsArray() @IsNotEmpty() @ValidateNested()
    optionalMax: I18n.Request.DefaultMax[] = [];
  }

  export class EditorType {
    @Expose() @Description('Channel')
    @Type(() => I18n.Request.DefaultMax)
    @IsArray() @ArrayNotEmpty() @IsNotEmpty() @ValidateNested()
    channel: I18n.Request.DefaultMax[] = [];

    @Expose() @Description('Payment')
    @Type(() => I18n.Request.DefaultMax)
    @IsArray() @ArrayNotEmpty() @IsNotEmpty() @ValidateNested()
    payment: I18n.Request.DefaultMax[] = [];
  }

  export class ImageMap {
    @Expose() @Description('coords')
    coords!: number[];

    @Expose() @Description('type')
    type!: GuideEnum.SHAPE.ENUM;

    @Expose() @Description('description')
    description!: string;

    constructor(options?: { coords?: number[]; type?: GuideEnum.SHAPE.ENUM; description?: string; }) {
      if (!!options) {
        if (!!options.coords) {
          this.coords = options.coords;
        }

        if (!!options.type) {
          this.type = options.type;
        }

        if (!!options.description) {
          this.description = options.description;
        }
      }
    }
  }
}
