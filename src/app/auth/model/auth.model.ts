import 'reflect-metadata';
import {Expose}               from 'class-transformer';
import {IsNotEmpty, IsString} from 'class-validator';

import {Description} from '@/core/decorator/description.decorator';

export namespace Auth {
  export namespace Request {
    export class Login {
      @Expose() @Description('사용자계정')
      @IsString() @IsNotEmpty()
      username!: string;

      @Expose() @Description('패스워드')
      @IsString() @IsNotEmpty()
      password!: string;
    }
  }
}
