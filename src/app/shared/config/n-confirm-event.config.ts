import {Enum} from '@/app/shared/enum/enum';

export const MESSAGE_EVENT_OPTIONS = {
  DEFAULT  : {
    title  : '적용 하시겠습니까?',
    actions: [
      {
        name    : '확인1',
        class   : 'btn btn-primary btn-sm',
        icon    : 'fas fa-angle-double-up',
        callback: 'ACTION1'
      },
      {
        name    : '확인2',
        class   : 'btn btn-success btn-sm',
        icon    : 'fas fa-angle-double-down',
        callback: 'ACTION2'
      }
    ],
    no     : {
      name : '취소',
      class: 'btn btn-secondary btn-sm',
      icon : 'fa fa-times'
    }
  },
  META_LIST: {
    title  : '적용 하시겠습니까?',
    actions: [
      {
        name    : '최상단',
        class   : 'btn btn-primary btn-sm',
        icon    : 'fas fa-angle-double-up',
        callback: Enum.CORE.ACTION.LIST_ACTIONS.TOP
      },
      {
        name    : '최하단',
        class   : 'btn btn-success btn-sm',
        icon    : 'fas fa-angle-double-down',
        callback: Enum.CORE.ACTION.LIST_ACTIONS.BOTTOM
      }
    ],
    no     : {
      name : '취소',
      class: 'btn btn-secondary btn-sm',
      icon : 'fa fa-times'
    }
  }
};
