import {MESSAGE} from '@/app/shared/config/message.config';
import {Toasts}  from '@/app/shared/model/toasts.model';

export const ERROR_MESSAGE = {
  ERROR        : (status: any): Toasts.Message =>
    new Toasts.Message({
      title   : MESSAGE.TOASTS.TITLE(status),
      contents: [MESSAGE.ERROR.DEFAULT, MESSAGE.TOASTS.ERROR(status)]
    }),
  ERROR_CODE   : (status: any, code: string): Toasts.Message =>
    new Toasts.Message({
      title   : MESSAGE.TOASTS.TITLE(status),
      contents: [`${code}: ${!!MESSAGE.SERVER[code] ? MESSAGE.SERVER[code] : MESSAGE.ERROR.DEFAULT}`]
    }),
  UNAUTHORIZED : (): Toasts.Message =>
    new Toasts.Message({
      title   : MESSAGE.TOASTS.TITLE('unauthorized'),
      contents: [MESSAGE.ERROR.TOKEN_TITLE, MESSAGE.TOASTS.ERROR(MESSAGE.ERROR.TOKEN_CONTENTS)]
    }),
  ERROR_500    : (status: any): Toasts.Message =>
    new Toasts.Message({
      title   : MESSAGE.TOASTS.TITLE(status),
      contents: [MESSAGE.ERROR.DEFAULT, MESSAGE.ERROR.CHECK_ADMIN]
    }),
  ERROR_DEFAULT: (status: any, error: any): Toasts.Message =>
    new Toasts.Message({
      title   : MESSAGE.TOASTS.TITLE(status),
      contents: [MESSAGE.ERROR.DEFAULT, MESSAGE.TOASTS.ERROR(error)]
    }),
};
