import {Shared} from '@/app/shared/model/shared.model';

export default class EnumService {
  isNumber(value: string): boolean {
    return !isNaN(Number(value));
  }

  getEnums(value: any): string[] {
    return Object.keys(value).map(key => value[key]);
  }

  getOptions(obj: any): Shared.Options[] {
    const returnValue: Shared.Options[] = [];
    const keys: string[] = this.getEnums(obj.ENUM);

    keys.forEach((key: string) => {
      returnValue.push({
        value: key,
        text : obj.NAME[key]
      });
    });

    return returnValue;
  }
}
