import {ClassConstructor} from 'class-transformer';

import {Enum}   from '@/app/shared/enum/enum';
import {Mapper} from '@/core/service/mapper.service';

export default class StorageService {
  private mapper = new Mapper();

  resetLocalStorage() {
    localStorage.removeItem(Enum.CORE.STORAGE.KEY.LIVE_ME);
    localStorage.removeItem(Enum.CORE.STORAGE.KEY.LIVE_MENU);
    localStorage.removeItem(Enum.CORE.STORAGE.KEY.LIVE_AUTH_TOKEN);
  }

  setLocalStorage(key: Enum.CORE.STORAGE.KEY, params: any) {
    if (!!params) {
      if (typeof params === Enum.CORE.STORAGE.TYPE.OBJECT) {
        localStorage.setItem(key, JSON.stringify(params));
      } else {
        localStorage.setItem(key, params);
      }
    }
  }

  getLocalStorage<T>(key: Enum.CORE.STORAGE.KEY, type?: Enum.CORE.STORAGE.TYPE, classType?: ClassConstructor<T>): any {
    let returnValue: any = null;
    const storage = localStorage.getItem(key);

    if (!!storage) {
      switch (type) {
        case Enum.CORE.STORAGE.TYPE.STRING:
          returnValue = storage;
          break;
        case Enum.CORE.STORAGE.TYPE.NUMBER:
          returnValue = parseInt(storage, 10);
          break;
        case Enum.CORE.STORAGE.TYPE.BOOLEAN:
          switch (storage) {
            case 'false':
              returnValue = false;
              break;
            case 'true':
              returnValue = true;
              break;
          }
          break;
        case Enum.CORE.STORAGE.TYPE.OBJECT:
          returnValue = !!classType ? this.mapper.toObject<T>(classType, JSON.parse(storage)) : JSON.parse(storage);
          break;
        case Enum.CORE.STORAGE.TYPE.ARRAY:
          returnValue = !!classType ? this.mapper.toArray(classType, JSON.parse(storage)) : JSON.parse(storage);
          break;
        default:
          returnValue = storage;
          break;
      }
    }

    return returnValue;
  }
}
