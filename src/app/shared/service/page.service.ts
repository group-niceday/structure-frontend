import {Pageable} from '@/app/shared/model/pageable.model';

export default class PageService {
  getPage<T>(search: Pageable.Request.Search, items: T[]): Pageable.Response.Page<T> {
    const page: Pageable.Response.Page<T> = new Pageable.Response.Page<T>();

    page.size = search.size;
    page.totalElements = items.length;

    page.totalPages = Math.floor(page.totalElements / search.size);
    page.number = search.page > page.totalPages ? page.totalPages : search.page;

    const from: number = page.number * page.size;
    const to: number = (page.number + 1) * page.size;
    page.content = items.slice(from, to);

    return page;
  }
}
