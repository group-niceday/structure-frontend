import {ClassConstructor}           from 'class-transformer';
import {ValidationError, Validator} from 'class-validator';

import {DEFAULT_LANGUAGE, FULL_REQUIRE_LANGUAGES, OPTION_LANGUAGES, REQUIRE_LANGUAGES} from '@/app/shared/config/i18n.config';
import {MESSAGE}                                                                       from '@/app/shared/config/message.config';
import {Enum}                                                                          from '@/app/shared/enum/enum';
import {Image}                                                                         from '@/app/shared/model/image.model';
import {I18n}                                                                          from '@/app/shared/model/i18n.model';
import {I18nFile}                                                                      from '@/app/shared/model/i18n-file.model';
import {Layout}                                                                        from '@/app/system/model/layout.model';
import {Mapper}                                                                        from '@/core/service/mapper.service';
import StorageService                                                                  from '@/app/shared/service/storage.service';

export default class I18nService {
  private mapper: Mapper = new Mapper();
  private validator = new Validator();
  private storageService = new StorageService();

  getState(obj: any): boolean | null {
    let returnValue: boolean | null = null;

    if (!this.isEmpty(obj)) {
      returnValue = true;

      const errors: ValidationError[] = this.validator.validateSync(obj);

      if (!!errors && errors.length > 0) {
        returnValue = false;
      }
    }

    return returnValue;
  }

  isEmpty(params: any): boolean {
    let returnValue = true;

    if (!!params &&
      (
        (params[Enum.CORE.I18N.LANG.ENUM.EN] !== null && params[Enum.CORE.I18N.LANG.ENUM.EN] !== undefined) ||
        (params[Enum.CORE.I18N.LANG.ENUM.KO] !== null && params[Enum.CORE.I18N.LANG.ENUM.KO] !== undefined)
      )
    ) {
      returnValue = false;
    }

    return returnValue;
  }

  getMessage(obj: any): string {
    let returnValue: string = '';

    if (!this.isEmpty(obj)) {
      const errors: ValidationError[] = this.validator.validateSync(obj);
      errors.forEach((error: ValidationError) => {
        const property = Enum.CORE.I18N.LANG.NAME[error.property];

        if (!!property) {
          if (!!returnValue) {
            returnValue = returnValue + '  ';
          }

          returnValue = returnValue + MESSAGE.WARNING.VALID_LANG_INPUT(property);
        }
      });
    }

    return returnValue;
  }

  getLanguage(): Enum.CORE.I18N.LANG.ENUM {
    let returnValue: Enum.CORE.I18N.LANG.ENUM = DEFAULT_LANGUAGE;

    const setting: Layout.SystemSetting = this.storageService.getLocalStorage(Enum.CORE.STORAGE.KEY.LIVE_SYSTEM, Enum.CORE.STORAGE.TYPE.OBJECT, Layout.SystemSetting);
    if (!!setting && !!setting.lang) {
      returnValue = setting.lang;
    }

    return returnValue;
  }

  getContent(models: I18n.Request.Default[]): string {
    let returnValue: string = '';
    const lang: Enum.CORE.I18N.LANG.ENUM = this.getLanguage();

    models.forEach((model: I18n.Request.Default) => {
      if (model.languageType === lang) {
        returnValue = model.content;
      }
    });

    return returnValue;
  }

  getImage(models: I18nFile.Response.Default[]): Image.Response.Default | null {
    let returnValue: Image.Response.Default | null = null;
    const lang: Enum.CORE.I18N.LANG.ENUM = this.getLanguage();

    models.forEach((model: I18nFile.Response.Default) => {
      if (model.languageType === lang) {
        returnValue = model.image;
      }
    });

    return returnValue;
  }

  isExist(languageType: Enum.CORE.I18N.LANG.ENUM, targets: I18n.Request.Default[] | I18nFile.Request.DefaultImage[]): boolean {
    let returnValue: boolean = false;

    targets.forEach((target: I18n.Request.Default | I18nFile.Request.DefaultImage) => {
      if (languageType === target.languageType) {
        returnValue = true;
      }
    });

    return returnValue;
  }

  getExistIndex(languageType: Enum.CORE.I18N.LANG.ENUM, targets: I18n.Request.Default[] | I18nFile.Request.DefaultImage[]): number | null {
    let returnValue: number | null = null;

    targets.forEach((target: I18n.Request.Default | I18nFile.Request.DefaultImage, index: number) => {
      if (languageType === target.languageType) {
        returnValue = index;
      }
    });

    return returnValue;
  }

  getInputClassModel(type: Enum.CORE.I18N.TYPE.INPUT): ClassConstructor<I18n.Input.Require | I18n.Input.FullRequire | I18n.Input.Optional | I18n.Input.RequireMax | I18n.Input.FullRequireMax | I18n.Input.OptionalMax> {
    let returnValue: ClassConstructor<I18n.Input.Require | I18n.Input.FullRequire | I18n.Input.Optional | I18n.Input.RequireMax | I18n.Input.FullRequireMax | I18n.Input.OptionalMax> = I18n.Input.Require;

    switch (type) {
      case Enum.CORE.I18N.TYPE.INPUT.REQUIRE:
        returnValue = I18n.Input.Require;
        break;
      case Enum.CORE.I18N.TYPE.INPUT.REQUIRE_MAX:
        returnValue = I18n.Input.RequireMax;
        break;

      case Enum.CORE.I18N.TYPE.INPUT.FULL_REQUIRE:
        returnValue = I18n.Input.FullRequire;
        break;
      case Enum.CORE.I18N.TYPE.INPUT.FULL_REQUIRE_MAX:
        returnValue = I18n.Input.FullRequireMax;
        break;

      case Enum.CORE.I18N.TYPE.INPUT.OPTIONAL:
        returnValue = I18n.Input.Optional;
        break;
      case Enum.CORE.I18N.TYPE.INPUT.OPTIONAL_MAX:
        returnValue = I18n.Input.OptionalMax;
        break;
    }

    return returnValue;
  }

  getInputClassParam(type: Enum.CORE.I18N.TYPE.INPUT): ClassConstructor<I18n.Request.Default | I18n.Request.DefaultMax> {
    let returnValue: ClassConstructor<I18n.Request.Default | I18n.Request.DefaultMax> = I18n.Request.Default;

    switch (type) {
      case Enum.CORE.I18N.TYPE.INPUT.REQUIRE:
      case Enum.CORE.I18N.TYPE.INPUT.FULL_REQUIRE:
      case Enum.CORE.I18N.TYPE.INPUT.OPTIONAL:
        returnValue = I18n.Request.Default;
        break;
      case Enum.CORE.I18N.TYPE.INPUT.REQUIRE_MAX:
      case Enum.CORE.I18N.TYPE.INPUT.FULL_REQUIRE_MAX:
      case Enum.CORE.I18N.TYPE.INPUT.OPTIONAL_MAX:
        returnValue = I18n.Request.DefaultMax;
        break;
    }

    return returnValue;
  }

  getInputModel(type: Enum.CORE.I18N.TYPE.INPUT): I18n.Input.Require | I18n.Input.FullRequire | I18n.Input.Optional | I18n.Input.RequireMax | I18n.Input.FullRequireMax | I18n.Input.OptionalMax | null {
    const classConstructor = this.getInputClassModel(type);
    return this.mapper.toObject(classConstructor, new classConstructor());
  }

  isLanguageInputRequire(type: Enum.CORE.I18N.TYPE.INPUT, lang: Enum.CORE.I18N.LANG.ENUM): boolean {
    let returnValue: boolean = false;
    let config: Enum.CORE.I18N.LANG.ENUM[] = OPTION_LANGUAGES;

    switch (type) {
      case Enum.CORE.I18N.TYPE.INPUT.REQUIRE:
      case Enum.CORE.I18N.TYPE.INPUT.REQUIRE_MAX:
        config = REQUIRE_LANGUAGES;
        break;
      case Enum.CORE.I18N.TYPE.INPUT.FULL_REQUIRE:
      case Enum.CORE.I18N.TYPE.INPUT.FULL_REQUIRE_MAX:
        config = FULL_REQUIRE_LANGUAGES;
        break;
      case Enum.CORE.I18N.TYPE.INPUT.OPTIONAL:
      case Enum.CORE.I18N.TYPE.INPUT.OPTIONAL_MAX:
        config = OPTION_LANGUAGES;
        break;
    }

    if (config.indexOf(lang) >= 0) {
      returnValue = true;
    }

    return returnValue;
  }

  getTextAreaClassModel(type: Enum.CORE.I18N.TYPE.TEXT_AREA): ClassConstructor<I18n.Input.Require | I18n.Input.FullRequire | I18n.Input.Optional | I18n.Input.RequireMax | I18n.Input.FullRequireMax | I18n.Input.OptionalMax> {
    let returnValue: ClassConstructor<I18n.Input.Require | I18n.Input.FullRequire | I18n.Input.Optional | I18n.Input.RequireMax | I18n.Input.FullRequireMax | I18n.Input.OptionalMax> = I18n.Input.Require;

    switch (type) {
      case Enum.CORE.I18N.TYPE.TEXT_AREA.REQUIRE:
        returnValue = I18n.Input.Require;
        break;
      case Enum.CORE.I18N.TYPE.TEXT_AREA.REQUIRE_MAX:
        returnValue = I18n.Input.RequireMax;
        break;

      case Enum.CORE.I18N.TYPE.TEXT_AREA.FULL_REQUIRE:
        returnValue = I18n.Input.FullRequire;
        break;
      case Enum.CORE.I18N.TYPE.TEXT_AREA.FULL_REQUIRE_MAX:
        returnValue = I18n.Input.FullRequireMax;
        break;

      case Enum.CORE.I18N.TYPE.TEXT_AREA.OPTIONAL:
        returnValue = I18n.Input.Optional;
        break;
      case Enum.CORE.I18N.TYPE.TEXT_AREA.OPTIONAL_MAX:
        returnValue = I18n.Input.OptionalMax;
        break;
    }

    return returnValue;
  }

  getTextAreaClassParam(type: Enum.CORE.I18N.TYPE.TEXT_AREA): ClassConstructor<I18n.Request.Default | I18n.Request.DefaultMax> {
    let returnValue: ClassConstructor<I18n.Request.Default | I18n.Request.DefaultMax> = I18n.Request.Default;

    switch (type) {
      case Enum.CORE.I18N.TYPE.TEXT_AREA.REQUIRE:
      case Enum.CORE.I18N.TYPE.TEXT_AREA.FULL_REQUIRE:
      case Enum.CORE.I18N.TYPE.TEXT_AREA.OPTIONAL:
        returnValue = I18n.Request.Default;
        break;
      case Enum.CORE.I18N.TYPE.TEXT_AREA.REQUIRE_MAX:
      case Enum.CORE.I18N.TYPE.TEXT_AREA.FULL_REQUIRE_MAX:
      case Enum.CORE.I18N.TYPE.TEXT_AREA.OPTIONAL_MAX:
        returnValue = I18n.Request.DefaultMax;
        break;
    }

    return returnValue;
  }

  getTextAreaModel(type: Enum.CORE.I18N.TYPE.TEXT_AREA): I18n.Input.Require | I18n.Input.FullRequire | I18n.Input.Optional | I18n.Input.RequireMax | I18n.Input.FullRequireMax | I18n.Input.OptionalMax | null {
    const classConstructor = this.getTextAreaClassModel(type);
    return this.mapper.toObject(classConstructor, new classConstructor());
  }

  isLanguageTextAreaRequire(type: Enum.CORE.I18N.TYPE.TEXT_AREA, lang: Enum.CORE.I18N.LANG.ENUM): boolean {
    let returnValue: boolean = false;
    let config: Enum.CORE.I18N.LANG.ENUM[] = OPTION_LANGUAGES;

    switch (type) {
      case Enum.CORE.I18N.TYPE.TEXT_AREA.REQUIRE:
      case Enum.CORE.I18N.TYPE.TEXT_AREA.REQUIRE_MAX:
        config = REQUIRE_LANGUAGES;
        break;
      case Enum.CORE.I18N.TYPE.TEXT_AREA.FULL_REQUIRE:
      case Enum.CORE.I18N.TYPE.TEXT_AREA.FULL_REQUIRE_MAX:
        config = FULL_REQUIRE_LANGUAGES;
        break;
      case Enum.CORE.I18N.TYPE.TEXT_AREA.OPTIONAL:
      case Enum.CORE.I18N.TYPE.TEXT_AREA.OPTIONAL_MAX:
        config = OPTION_LANGUAGES;
        break;
    }

    if (config.indexOf(lang) >= 0) {
      returnValue = true;
    }

    return returnValue;
  }

  getEditorClassModel(type: Enum.CORE.I18N.TYPE.EDITOR): ClassConstructor<I18n.Input.Require | I18n.Input.FullRequire | I18n.Input.Optional | I18n.Input.RequireMax | I18n.Input.FullRequireMax | I18n.Input.OptionalMax> {
    let returnValue: ClassConstructor<I18n.Input.Require | I18n.Input.FullRequire | I18n.Input.Optional | I18n.Input.RequireMax | I18n.Input.FullRequireMax | I18n.Input.OptionalMax> = I18n.Input.Require;

    switch (type) {
      case Enum.CORE.I18N.TYPE.EDITOR.REQUIRE:
        returnValue = I18n.Input.Require;
        break;
      case Enum.CORE.I18N.TYPE.EDITOR.REQUIRE_MAX:
        returnValue = I18n.Input.RequireMax;
        break;

      case Enum.CORE.I18N.TYPE.EDITOR.FULL_REQUIRE:
        returnValue = I18n.Input.FullRequire;
        break;
      case Enum.CORE.I18N.TYPE.EDITOR.FULL_REQUIRE_MAX:
        returnValue = I18n.Input.FullRequireMax;
        break;

      case Enum.CORE.I18N.TYPE.EDITOR.OPTIONAL:
        returnValue = I18n.Input.Optional;
        break;
      case Enum.CORE.I18N.TYPE.EDITOR.OPTIONAL_MAX:
        returnValue = I18n.Input.OptionalMax;
        break;
    }

    return returnValue;
  }

  getEditorClassParam(type: Enum.CORE.I18N.TYPE.EDITOR): ClassConstructor<I18n.Request.Default | I18n.Request.DefaultMax> {
    let returnValue: ClassConstructor<I18n.Request.Default | I18n.Request.DefaultMax> = I18n.Request.Default;

    switch (type) {
      case Enum.CORE.I18N.TYPE.EDITOR.REQUIRE:
      case Enum.CORE.I18N.TYPE.EDITOR.FULL_REQUIRE:
      case Enum.CORE.I18N.TYPE.EDITOR.OPTIONAL:
        returnValue = I18n.Request.Default;
        break;
      case Enum.CORE.I18N.TYPE.EDITOR.REQUIRE_MAX:
      case Enum.CORE.I18N.TYPE.EDITOR.FULL_REQUIRE_MAX:
      case Enum.CORE.I18N.TYPE.EDITOR.OPTIONAL_MAX:
        returnValue = I18n.Request.DefaultMax;
        break;
    }

    return returnValue;
  }

  getEditorModel(type: Enum.CORE.I18N.TYPE.EDITOR): I18n.Input.Require | I18n.Input.FullRequire | I18n.Input.Optional | I18n.Input.RequireMax | I18n.Input.FullRequireMax | I18n.Input.OptionalMax | null {
    const classConstructor = this.getEditorClassModel(type);
    return this.mapper.toObject(classConstructor, new classConstructor());
  }

  isLanguageEditorRequire(type: Enum.CORE.I18N.TYPE.EDITOR, lang: Enum.CORE.I18N.LANG.ENUM): boolean {
    let returnValue: boolean = false;
    let config: Enum.CORE.I18N.LANG.ENUM[] = OPTION_LANGUAGES;

    switch (type) {
      case Enum.CORE.I18N.TYPE.EDITOR.REQUIRE:
      case Enum.CORE.I18N.TYPE.EDITOR.REQUIRE_MAX:
        config = REQUIRE_LANGUAGES;
        break;
      case Enum.CORE.I18N.TYPE.EDITOR.FULL_REQUIRE:
      case Enum.CORE.I18N.TYPE.EDITOR.FULL_REQUIRE_MAX:
        config = FULL_REQUIRE_LANGUAGES;
        break;
      case Enum.CORE.I18N.TYPE.EDITOR.OPTIONAL:
      case Enum.CORE.I18N.TYPE.EDITOR.OPTIONAL_MAX:
        config = OPTION_LANGUAGES;
        break;
    }

    if (config.indexOf(lang) >= 0) {
      returnValue = true;
    }

    return returnValue;
  }

  getImageClassModel(type: Enum.CORE.I18N.TYPE.IMAGE): ClassConstructor<I18nFile.Input.Require | I18nFile.Input.FullRequire | I18nFile.Input.Optional> {
    let returnValue: ClassConstructor<I18nFile.Input.Require | I18nFile.Input.FullRequire | I18nFile.Input.Optional> = I18nFile.Input.Require;

    switch (type) {
      case Enum.CORE.I18N.TYPE.IMAGE.REQUIRE:
        returnValue = I18nFile.Input.Require;
        break;

      case Enum.CORE.I18N.TYPE.IMAGE.FULL_REQUIRE:
        returnValue = I18nFile.Input.FullRequire;
        break;

      case Enum.CORE.I18N.TYPE.IMAGE.OPTIONAL:
        returnValue = I18nFile.Input.Optional;
        break;
    }

    return returnValue;
  }

  getImageClassParam(type: Enum.CORE.I18N.TYPE.IMAGE): ClassConstructor<I18nFile.Request.DefaultImage> {
    let returnValue: ClassConstructor<I18nFile.Request.DefaultImage> = I18nFile.Request.DefaultImage;

    switch (type) {
      case Enum.CORE.I18N.TYPE.IMAGE.REQUIRE:
      case Enum.CORE.I18N.TYPE.IMAGE.FULL_REQUIRE:
      case Enum.CORE.I18N.TYPE.IMAGE.OPTIONAL:
        returnValue = I18nFile.Request.DefaultImage;
        break;
    }

    return returnValue;
  }

  getImageModel(type: Enum.CORE.I18N.TYPE.IMAGE): I18nFile.Input.Require | I18nFile.Input.FullRequire | I18nFile.Input.Optional | null {
    const classConstructor = this.getImageClassModel(type);
    return this.mapper.toObject(classConstructor, new classConstructor());
  }

  isLanguageImageRequire(type: Enum.CORE.I18N.TYPE.IMAGE, lang: Enum.CORE.I18N.LANG.ENUM): boolean {
    let returnValue: boolean = false;
    let config: Enum.CORE.I18N.LANG.ENUM[] = OPTION_LANGUAGES;

    switch (type) {
      case Enum.CORE.I18N.TYPE.IMAGE.REQUIRE:
        config = REQUIRE_LANGUAGES;
        break;
      case Enum.CORE.I18N.TYPE.IMAGE.FULL_REQUIRE:
        config = FULL_REQUIRE_LANGUAGES;
        break;
      case Enum.CORE.I18N.TYPE.IMAGE.OPTIONAL:
        config = OPTION_LANGUAGES;
        break;
    }

    if (config.indexOf(lang) >= 0) {
      returnValue = true;
    }

    return returnValue;
  }
}
