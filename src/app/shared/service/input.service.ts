import _                            from 'lodash';
import {ValidationError, Validator} from 'class-validator';

import {MESSAGE} from '@/app/shared/config/message.config';

export default class InputService {
  private validator = new Validator();

  getState(target: string, obj: any): boolean | null {
    let returnValue: boolean | null = null;
    if (obj[target] !== undefined) {
      returnValue = true;

      const errors: ValidationError[] = this.validator.validateSync(obj);
      errors.forEach((error: ValidationError) => {
        if (!!error && error.property === target) {
          returnValue = false;
        }
      });
    }

    return returnValue;
  }

  getStateAll(obj: any): boolean | null {
    let returnValue: boolean | null = null;
    if (!!obj) {
      returnValue = true;

      const errors: ValidationError[] = this.validator.validateSync(obj);
      if (!errors || (!!errors && errors.length > 0)) {
        returnValue = false;
      }
    }

    return returnValue;
  }

  getMessage(target: string, obj: any): string {
    let returnValue: string = '';

    if (obj[target] !== undefined) {
      const errors: ValidationError[] = this.validator.validateSync(obj);
      errors.forEach((error: ValidationError) => {
        if (!!error && error.property === target) {
          _.keys(error.constraints).forEach((key: string) => {
            if (!!MESSAGE.VALIDATOR[key]) {
              returnValue = this.getStrArrToMessage(returnValue, MESSAGE.VALIDATOR[key]);
            } else {
              returnValue = this.getStrArrToMessage(returnValue, !!error.constraints ? error.constraints[key] : '');
            }
          });
        }
      });
    }

    return returnValue;
  }

  getStateOfNull(target: string, obj: any): boolean | null {
    let returnValue: boolean | null = null;
    if (obj[target] !== null && obj[target] !== undefined) {
      returnValue = true;

      const errors: ValidationError[] = this.validator.validateSync(obj);
      errors.forEach((error: ValidationError) => {
        if (!!error && error.property === target) {
          returnValue = false;
        }
      });
    }

    return returnValue;
  }

  getMessageOfNull(target: string, obj: any): string {
    let returnValue: string = '';

    if (obj[target] !== null && obj[target] !== undefined) {
      const errors: ValidationError[] = this.validator.validateSync(obj);
      errors.forEach((error: ValidationError) => {
        if (!!error && error.property === target) {
          _.keys(error.constraints).forEach((key: string) => {
            if (!!MESSAGE.VALIDATOR[key]) {
              returnValue = this.getStrArrToMessage(returnValue, MESSAGE.VALIDATOR[key]);
            } else {
              returnValue = this.getStrArrToMessage(returnValue, !!error.constraints ? error.constraints[key] : '');
            }
          });
        }
      });
    }

    return returnValue;
  }

  getStrArrToMessage(returnValue: string, value: string): string {
    if (!!returnValue) {
      returnValue = returnValue + '  ';
    }

    returnValue = returnValue + value;

    return returnValue;
  }
}
