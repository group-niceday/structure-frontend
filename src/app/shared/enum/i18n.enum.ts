export namespace I18nEnum {
  export namespace TYPE {
    export enum INPUT {
      REQUIRE = 'REQUIRE',
      REQUIRE_MAX = 'REQUIRE_MAX',

      FULL_REQUIRE = 'FULL_REQUIRE',
      FULL_REQUIRE_MAX = 'FULL_REQUIRE_MAX',

      OPTIONAL = 'OPTIONAL',
      OPTIONAL_MAX = 'OPTIONAL_MAX',
    }

    export enum TEXT_AREA {
      REQUIRE = 'REQUIRE',
      REQUIRE_MAX = 'REQUIRE_MAX',

      FULL_REQUIRE = 'FULL_REQUIRE',
      FULL_REQUIRE_MAX = 'FULL_REQUIRE_MAX',

      OPTIONAL = 'OPTIONAL',
      OPTIONAL_MAX = 'OPTIONAL_MAX',
    }

    export enum IMAGE {
      REQUIRE = 'REQUIRE',
      FULL_REQUIRE = 'FULL_REQUIRE',
      OPTIONAL = 'OPTIONAL',
    }

    export enum EDITOR {
      REQUIRE = 'REQUIRE',
      REQUIRE_MAX = 'REQUIRE_MAX',

      FULL_REQUIRE = 'FULL_REQUIRE',
      FULL_REQUIRE_MAX = 'FULL_REQUIRE_MAX',

      OPTIONAL = 'OPTIONAL',
      OPTIONAL_MAX = 'OPTIONAL_MAX',
    }
  }

  export namespace LANG {
    export enum ENUM {
      EN = 'EN',
      KO = 'KO',
    }

    export enum NAME {
      EN = '영어',
      KO = '한국어',
    }

    export enum NAME_SHORT {
      EN = 'A',
      KO = '가',
    }
  }
}
