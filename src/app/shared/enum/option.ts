import {Shared} from '@/app/shared/model/shared.model';

export namespace Option {
  export namespace BOOLEAN {
    export const COMMON: Shared.Options[] = [
      new Shared.Options('예', true),
      new Shared.Options('아니오', false),
    ];
  }
}
