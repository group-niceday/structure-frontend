import {Getters} from 'vuex-smart-module';

import {Enum}           from '@/app/shared/enum/enum';
import {ConditionState} from '@/app/shared/vuex/condition/condition.state';

export class ConditionGetter extends Getters<ConditionState> {
  get(key: Enum.CORE.ACTION.CONDITION) {
    return this.state.condition[key];
  }
}
