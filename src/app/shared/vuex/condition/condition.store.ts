import {createStore, Module} from 'vuex-smart-module';

import {ConditionState}    from '@/app/shared/vuex/condition/condition.state';
import {ConditionGetter}   from '@/app/shared/vuex/condition/condition.getter';
import {ConditionMutation} from '@/app/shared/vuex/condition/condition.mutation';
import {ConditionAction}   from '@/app/shared/vuex/condition/condition.action';


const context = new Module({
  state    : ConditionState,
  getters  : ConditionGetter,
  mutations: ConditionMutation,
  actions  : ConditionAction
});

export class ConditionStore {
  static getInstance() {
    return context.context(createStore(context));
  }
}
