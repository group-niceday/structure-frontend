import {AxiosResponse} from 'axios';
import {Actions}       from 'vuex-smart-module';

import {Enum}         from '@/app/shared/enum/enum';
import {Image}        from '@/app/shared/model/image.model';
import {FileState}    from '@/app/shared/vuex/file/file.state';
import {FileGetter}   from '@/app/shared/vuex/file/file.getter';
import {FileMutation} from '@/app/shared/vuex/file/file.mutation';
import AxiosService   from '@/app/shared/service/axios.service';

export class FileAction extends Actions<FileState, FileGetter, FileMutation, FileAction> {
  private http = new AxiosService(`${process.env.VUE_APP_API_URL}`);

  addImage(params: { target: string; formData: FormData; }) {
    this.http.post(
      `/images`,
      params.formData,
      {
        headers: {
          'Content-type': 'multipart/form-data'
        }
      }
    ).then(
      (response: AxiosResponse<Image.Response.Default>) => {
        const result: Image.Response.Result = new Image.Response.Result(params.target, Enum.CORE.ACTION.RESULT.ADD);
        result.image = response.data;
        this.mutations.setResultImage(result);
      }
    );
  }

  deleteImage(params: { target: string; file: Image.Request.Default; }) {
    // this.http.delete(
    //   `/images/${params.file.id}`
    // ).then(
    //   () => {
    //     const result: Image.Response.Result = new Image.Response.Result(params.target, Enum.CORE.ACTION.RESULT.DELETE);
    //     this.mutations.setResultImage(result);
    //   }
    // );

    const result: Image.Response.Result = new Image.Response.Result(params.target, Enum.CORE.ACTION.RESULT.DELETE);
    this.mutations.setResultImage(result);
  }
}
