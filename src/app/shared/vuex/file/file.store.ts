import {createStore, Module} from 'vuex-smart-module';

import {FileState}    from '@/app/shared/vuex/file/file.state';
import {FileGetter}   from '@/app/shared/vuex/file/file.getter';
import {FileMutation} from '@/app/shared/vuex/file/file.mutation';
import {FileAction}   from '@/app/shared/vuex/file/file.action';

const context = new Module({
  state    : FileState,
  getters  : FileGetter,
  mutations: FileMutation,
  actions  : FileAction
});

const Store = createStore(context);

export class FileStore {
  static getInstance() {
    return context.context(Store);
  }
}
