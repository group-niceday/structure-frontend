import {Mutations} from 'vuex-smart-module';

import {Image}     from '@/app/shared/model/image.model';
import {Mapper}    from '@/core/service/mapper.service';
import {FileState} from '@/app/shared/vuex/file/file.state';

export class FileMutation extends Mutations<FileState> {
  private mapper = new Mapper();

  setResultImage(result: Image.Response.Result) {
    this.state.resultImage = this.mapper.toObject(Image.Response.Result, result);
  }
}
