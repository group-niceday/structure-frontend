export namespace DateTime {
  export class Callback {
    keys!: string;
    index!: number;
    date!: string | null;
  }

  export class RangeCallback {
    keys!: RangeProps;
    startDate!: string | null;
    endDate!: string | null;
  }

  export class RangeProps {
    startDate!: string;
    endDate!: string;
    startIndex!: number;
    endIndex!: number;
  }
}
