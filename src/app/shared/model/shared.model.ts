import 'reflect-metadata';
import {Expose} from 'class-transformer';

import {Description} from '@/core/decorator/description.decorator';
import {Enum}        from '@/app/shared/enum/enum';

export namespace Shared {
  export namespace Response {
    export class Author {
      @Expose() @Description('등록일시')
      createdAt!: string;
    }

    export class ActionResult {
      @Expose()
      action!: Enum.CORE.ACTION.RESULT | null;

      @Expose()
      data!: any | null;
    }
  }

  export class Options {
    @Expose() @Description('OPTIONS_KEY')
    value!: string | number | boolean | null;

    @Expose() @Description('OPTIONS_VALUE')
    text!: string;

    @Expose() @Description('OPTIONS_DISABLED')
    disabled?: boolean = false;

    constructor(text?: string, value?: string | number | boolean | null, disabled?: boolean) {
      if (!!text) {
        this.text = text;
      }

      if (value !== null && value !== undefined) {
        this.value = value;
      }

      if (disabled !== null && disabled !== undefined) {
        this.disabled = disabled;
      }
    }
  }
}
