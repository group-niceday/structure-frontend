import {Expose, Type}                                                from 'class-transformer';
import {IsNotEmpty, IsOptional, IsString, MaxLength, ValidateNested} from 'class-validator';

import {Description} from '@/core/decorator/description.decorator';
import {Enum}        from '@/app/shared/enum/enum';
import {Image}       from '@/app/shared/model/image.model';

export namespace I18nFile {
  export namespace Request {
    export class DefaultImage {
      @Expose() @Description('언어')
      @IsString() @IsNotEmpty() @MaxLength(50)
      languageType!: Enum.CORE.I18N.LANG.ENUM;

      @Expose() @Description('내용')
      @Type(() => Image.Request.Default)
      @IsNotEmpty() @ValidateNested()
      image!: Image.Request.Default | null;

      constructor(languageType: Enum.CORE.I18N.LANG.ENUM, image?: Image.Request.Default | null) {
        this.languageType = languageType;
        if (!!image) {
          this.image = image;
        }
      }
    }
  }

  export namespace Response {
    export class Default {
      @Expose() @Description('영어')
      languageType!: Enum.CORE.I18N.LANG.ENUM;

      @Expose() @Description('한국어')
      image!: Image.Request.Default | null;
    }
  }

  export namespace Input {
    interface CheckI18n {
      EN: Image.Request.Default | null;
      KO: Image.Request.Default | null;
    }

    export class Display implements CheckI18n {
      @Expose() @Description('영어')
      EN!: Image.Request.Default | null;

      @Expose() @Description('한국어')
      KO!: Image.Request.Default | null;
    }

    export class Require implements CheckI18n {
      @Expose() @Description('영어')
      @IsNotEmpty() @ValidateNested()
      EN!: Image.Request.Default | null;

      @Expose() @Description('한국어')
      @IsOptional() @ValidateNested()
      KO!: Image.Request.Default | null;
    }

    export class FullRequire implements CheckI18n {
      @Expose() @Description('영어')
      @IsNotEmpty() @ValidateNested()
      EN!: Image.Request.Default | null;

      @Expose() @Description('한국어')
      @IsNotEmpty() @ValidateNested()
      KO!: Image.Request.Default | null;
    }

    export class Optional implements CheckI18n {
      @Expose() @Description('영어')
      @IsOptional() @ValidateNested()
      EN!: Image.Request.Default | null;

      @Expose() @Description('한국어')
      @IsOptional() @ValidateNested()
      KO!: Image.Request.Default | null;
    }
  }
}
