import Vue from 'vue';

import {Enum}      from '@/app/shared/enum/enum';
import {Shared}    from '@/app/shared/model/shared.model';
import UtilService from '@/app/shared/service/util.service';

const $util = new UtilService();

Vue.filter('enumCodeFilter', (value: string, enums: any, nullTag?: string) => {
  let valueNm = value;

  if (!!value && !!enums && !!enums.NAME) {
    valueNm = enums.NAME[value];
  }

  if (!value && !!nullTag) {
    valueNm = nullTag;
  }

  return valueNm;
});

Vue.filter('dateFilter', (value: string) => {
  let valueNm = value;

  if (!!value) {
    valueNm = $util.dateToFormatDate(value, Enum.CORE.FORM.DATE_FORMAT.DISPLAY_DATE_TIME);
  }

  return valueNm;
});

Vue.filter('dateSecondFilter', (value: string) => {
  let valueNm = value;

  if (!!value) {
    valueNm = $util.dateToFormatDate(value, Enum.CORE.FORM.DATE_FORMAT.DISPLAY_DATE_TIME_SECOND);
  }

  return valueNm;
});

Vue.filter('navigationFilter', (value: string[]) => {
  let valueNm = '';

  value.forEach((item: string) => {
    if (!!valueNm) {
      valueNm = `${valueNm} > ${item}`;
    } else {
      valueNm = `${item}`;
    }
  });

  return valueNm;
});

Vue.filter('phoneNumberFilter', (value: string) => {
  let valueNm = value;

  if (!!value && value.length > 8) {
    const top = value.slice(0, value.length - 8);
    const middle = value.slice(value.length - 8, value.length - 4);
    const low = value.slice(value.length - 4, value.length);

    valueNm = `${top}-${middle}-${low}`;
  }

  return valueNm;
});

Vue.filter('optionFilter', (value: boolean | null, options: Shared.Options[]) => {
  let valueNm = '';

  if (!!options && options.length > 0) {

    options.forEach((option: Shared.Options) => {

      if (option.value === value) {
        valueNm = option.text;
      }
    });
  }

  return valueNm;
});

Vue.filter('useYnFilter', (value: boolean | null) => {
  let valueNm = '';

  if (!!value) {
    valueNm = '사용';
  } else if (!value) {
    valueNm = '미사용';
  }

  return valueNm;
});

Vue.filter('displayFilter', (id: string | number | null | undefined, name: string | null | undefined) => {
  return $util.getIdNameToDisplay(id, name);
});

Vue.filter('connectionFilter', (value: object, options?: { id?: string; name?: string; }) => {
  let valueNm: string | object = '';

  if (!!options) {
    if (!options.id) {
      options.id = 'id';
    }

    if (!options.name) {
      options.name = 'name';
    }
  } else {
    options = {id: 'id', name: 'name'};
  }

  if (!!options && !!options.id && !!options.name) {
    if (!!value && !!value[options.id] && !!value[options.name]) {
      valueNm = `(${value[options.id]}) ${value[options.name]}`;
    }
  }

  return valueNm;
});

Vue.filter('commaFilter', (value: string | number | null, prefix?: string) => {
  let returnValue: string | number | null = value;

  if (!$util.isEmpty(value)) {
    returnValue = String(value).replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    if (!!prefix) {
      returnValue = prefix + returnValue;
    }
  }

  return returnValue;
});

Vue.filter('textareaFilter', (value: any) => {
  return value.replace(/(?:\r\n|\r|\n)/g, '<br/>');
});

Vue.filter('jsonPrettyFilter', (value: any) => {
  return JSON.stringify(value, null, 2);
});
