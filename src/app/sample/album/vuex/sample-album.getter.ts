import {Getters} from 'vuex-smart-module';

import {Shared}           from '@/app/shared/model/shared.model';
import {SampleAlbum}      from '@/app/sample/album/model/sample-album.model';
import {SampleAlbumState} from '@/app/sample/album/vuex/sample-album.state';
import {Mapper}           from '@/core/service/mapper.service';

export class SampleAlbumGetter extends Getters<SampleAlbumState> {
  private mapper = new Mapper();

  getList(): SampleAlbum.Response.FindAll[] {
    return this.mapper.toArray<SampleAlbum.Response.FindAll>(SampleAlbum.Response.FindAll, this.state.list);
  }

  getOne(): SampleAlbum.Response.FindOne {
    return this.mapper.toObject<SampleAlbum.Response.FindOne>(SampleAlbum.Response.FindOne, this.state.one);
  }

  getModify(): SampleAlbum.Request.Modify {
    return this.mapper.toObject<SampleAlbum.Request.Modify>(SampleAlbum.Request.Modify, this.state.one);
  }

  getResult(): Shared.Response.ActionResult {
    return this.mapper.toObject<Shared.Response.ActionResult>(Shared.Response.ActionResult, this.state.result);
  }
}
