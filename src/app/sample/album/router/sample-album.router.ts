const SampleAlbumRouter = [
  {
    path     : '/sample/album',
    component: () => import (/* webpackChunkName: "sample.album" */ '@/app/system/view/default-layout.vue'),
    children : [
      {
        path     : 'list',
        meta     : {routerGuard: true},
        component: () => import(/* webpackChunkName: "sample.album" */ '@/app/sample/album/view/sample-album-list.page.vue')
      },
      {
        path     : 'detail/:id',
        meta     : {routerGuard: true},
        component: () => import(/* webpackChunkName: "sample.album" */ '@/app/sample/album/view/sample-album-detail.page.vue')
      },
      {
        path     : 'add',
        meta     : {routerGuard: true},
        component: () => import(/* webpackChunkName: "sample.album" */ '@/app/sample/album/view/sample-album-add.page.vue')
      },
      {
        path     : 'modify/:id',
        meta     : {routerGuard: true},
        component: () => import(/* webpackChunkName: "sample.album" */ '@/app/sample/album/view/sample-album-modify.page.vue')
      }
    ],
    redirect : '/sample/album/list'
  }
];

export default SampleAlbumRouter;
