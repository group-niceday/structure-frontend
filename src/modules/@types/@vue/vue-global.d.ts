import Vue from 'vue';

import UtilService from '@/app/shared/service/util.service';
import {Mapper}    from '@/core/service/mapper.service';
import {Broadcast} from '@/core/service/broadcast.service';

declare module 'vue/types/vue' {
  interface Vue {
    $enum: any;
    $option: any;
    $message: any;
    $util: UtilService;
    $broadcast: Broadcast;
    $mapper: Mapper;
  }

  interface VueConstructor {
    $enum: any;
    $option: any;
    $message: any;
    $util: UtilService;
    $broadcast: Broadcast;
    $mapper: Mapper;
  }
}

declare module 'vue/types/options' {
  interface ComponentOptions<V extends Vue> {
    $enum?: any;
    $option?: any;
    $message?: any;
    $util?: UtilService;
    $broadcast?: Broadcast;
    $mapper?: Mapper;
  }
}
