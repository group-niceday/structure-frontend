import 'babel-polyfill';
import Vue          from 'vue';
import Vuex         from 'vuex';
import VueRouter    from 'vue-router';
import BootstrapVue from 'bootstrap-vue';
import Sticky       from 'vue-sticky-directive';

import App         from '@/app.vue';
import router      from '@/router';
import Components  from '@/components';
import {MESSAGE}   from '@/app/shared/config/message.config';
import {Enum}      from '@/app/shared/enum/enum';
import {Option}    from '@/app/shared/enum/option';
import UtilService from '@/app/shared/service/util.service';
import {Broadcast} from '@/core/service/broadcast.service';
import {Mapper}    from '@/core/service/mapper.service';

import '@/app/shared/filter/shared.filter';
import '@/app/shared/directive/shared.directive';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'verte/dist/verte.css';
import 'vue2-datepicker/index.css';
import 'vue-prism-editor/dist/prismeditor.min.css';
import 'prismjs/themes/prism.css';

import '@/assets/css/side-bar.css';
import '@/assets/css/styles.css';
import '@/assets/css/layout.css';
import '@/assets/css/code-editor.scss';

Vue.config.devtools = process.env.NODE_ENV === Enum.CORE.ENV.LOCAL;
Vue.config.performance = process.env.NODE_ENV === Enum.CORE.ENV.LOCAL;
Vue.config.silent = process.env.NODE_ENV === Enum.CORE.ENV.PROD;
Vue.config.productionTip = false;

Vue.prototype.$enum = Enum;
Vue.prototype.$option = Option;
Vue.prototype.$message = MESSAGE;
Vue.prototype.$util = new UtilService();
Vue.prototype.$broadcast = new Broadcast();
Vue.prototype.$mapper = new Mapper();

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(Sticky);
Vue.use(Components);

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
