import {registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface} from 'class-validator';

import {MESSAGE} from '@/app/shared/config/message.config';

export function IsRelativeUrl(validationOptions?: ValidationOptions) {
  return (target: object, propertyName: string) => {
    registerDecorator({
      target     : target.constructor,
      propertyName,
      options    : validationOptions,
      constraints: [],
      validator  : IsRelativeUrlValidate,
    });
  };
}

@ValidatorConstraint()
class IsRelativeUrlValidate implements ValidatorConstraintInterface {
  validate(url: string, args: ValidationArguments): boolean {
    let returnValue: boolean = false;

    if (!!url) {
      returnValue = (/^\/[a-z0-9._-]*$/g).test(url);
    }

    return returnValue;
  }

  defaultMessage(args: ValidationArguments) {
    return MESSAGE.WARNING.RELATIVE_URL;
  }
}

export function IsAbsoluteUrl(validationOptions?: ValidationOptions) {
  return (target: object, propertyName: string) => {
    registerDecorator({
      target     : target.constructor,
      propertyName,
      options    : validationOptions,
      constraints: [],
      validator  : IsAbsoluteUrlValidate,
    });
  };
}

@ValidatorConstraint()
class IsAbsoluteUrlValidate implements ValidatorConstraintInterface {
  validate(url: string, args: ValidationArguments): boolean {
    let returnValue: boolean = false;

    if (!!url) {
      returnValue = (/^https?:\/\/(.*)$/).test(url);
    }

    return returnValue;
  }

  defaultMessage(args: ValidationArguments) {
    return MESSAGE.WARNING.ABSOLUTE_URL;
  }
}
