import _                                                                                                              from 'lodash';
import {registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface} from 'class-validator';

import {REQUIRE_LANGUAGES} from '@/app/shared/config/i18n.config';
import {Enum}              from '@/app/shared/enum/enum';
import {I18n}              from '@/app/shared/model/i18n.model';

function getDifference(i18ns: any[], lang: Enum.CORE.I18N.LANG.ENUM[]): Enum.CORE.I18N.LANG.ENUM[] {
  const languageTypes: Enum.CORE.I18N.LANG.ENUM[] = lang;
  const nowLanguageTypes: Enum.CORE.I18N.LANG.ENUM[] = [];
  i18ns.forEach((i18n: any) => {
    nowLanguageTypes.push(i18n.languageType);
  });

  return _.difference(languageTypes, nowLanguageTypes);
}

function getLanguage(langs: Enum.CORE.I18N.LANG.ENUM[]): string {
  let langStr: string = '';
  langs.forEach((lang: Enum.CORE.I18N.LANG.ENUM) => {
    if (!!langStr) {
      langStr = ', ' + langStr;
    }

    langStr = `${Enum.CORE.I18N.LANG.NAME[lang]}` + langStr;
  });

  return langStr;
}

export function IsI18n(allows: Enum.CORE.I18N.LANG.ENUM[] = REQUIRE_LANGUAGES, validationOptions?: ValidationOptions) {
  return (target: object, propertyName: string) => {
    registerDecorator({
      target     : target.constructor,
      propertyName,
      options    : validationOptions,
      constraints: [allows],
      validator  : IsI18nValidate,
    });
  };
}

@ValidatorConstraint()
export class IsI18nValidate implements ValidatorConstraintInterface {
  validate(i18ns: I18n.Request.Default[] | I18n.Request.DefaultMax[] | null, args: ValidationArguments): boolean {
    let returnValue: boolean = false;

    if (!!i18ns) {
      const differenceLanguageTypes: Enum.CORE.I18N.LANG.ENUM[] = getDifference(args.value, args.constraints[0]);
      if (differenceLanguageTypes.length === 0) {
        returnValue = true;
      }
    }

    return returnValue;
  }

  defaultMessage(args: ValidationArguments) {
    let returnValue: string = '';

    if (!!args.value) {
      const differenceLanguageTypes: Enum.CORE.I18N.LANG.ENUM[] = getDifference(args.value, args.constraints[0]);
      returnValue = `다국어 ${getLanguage(differenceLanguageTypes)} 정보는 필수 정보 입니다.`;
    }

    return returnValue;
  }
}
