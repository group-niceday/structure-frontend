import _                            from 'lodash';
import {ValidationError, Validator} from 'class-validator';

import {Broadcast} from '@/core/service/broadcast.service';

export function Validate(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
  const broadcast = new Broadcast();
  const validator = new Validator();
  const original = descriptor.value;
  descriptor.value = function(...args) {
    return validator.validate(args[0]).then((errors: ValidationError[]) => {

      if (_.isEmpty(errors)) {
        return original.apply(this, args);
      }

      broadcast.onValidate(errors);
      return false;
    });
  };
}
