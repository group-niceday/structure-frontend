import Vue from 'vue';

import {Enum}   from '@/app/shared/enum/enum';
import {Toasts} from '@/app/shared/model/toasts.model';

export const broadcast = new Vue();

export class Broadcast {
  onSettingChange() {
    broadcast.$emit(
      Enum.CORE.ACTION.BROADCAST.SYSTEM_SETTING_CHANGE
    );
  }

  onToastsError(contents: string | string[]) {
    broadcast.$emit(
      Enum.CORE.ACTION.BROADCAST.TOAST_VALIDATE,
      new Toasts.Message({variant: Enum.CORE.TOASTS.VARIANT.primary, title: '오류', contents: (typeof contents === 'string') ? [contents] : contents})
    );
  }

  onToasts(variant: Enum.CORE.TOASTS.VARIANT, title: string, contents: string | string[]) {
    broadcast.$emit(
      Enum.CORE.ACTION.BROADCAST.TOAST_MESSAGE,
      new Toasts.Message({variant, title, contents: (typeof contents === 'string') ? [contents] : contents})
    );
  }

  onValidate(error: any) {
    broadcast.$emit(
      Enum.CORE.ACTION.BROADCAST.TOAST_VALIDATE,
      error
    );
  }

  onDefaultImageViewer(url: string) {
    broadcast.$emit(
      Enum.CORE.ACTION.BROADCAST.IMAGE_VIEWER,
      url
    );
  }

  onDefaultSpinner(useYn: boolean) {
    broadcast.$emit(
      Enum.CORE.ACTION.BROADCAST.SPINNER_DEFAULT,
      useYn
    );
  }

  onWriteSpinner(useYn: boolean) {
    broadcast.$emit(
      Enum.CORE.ACTION.BROADCAST.SPINNER_WRITE,
      useYn
    );
  }

  onError(error: any) {
    broadcast.$emit(
      Enum.CORE.ACTION.BROADCAST.SPINNER_ERROR,
      error
    );

    broadcast.$emit(
      Enum.CORE.ACTION.BROADCAST.TOASTS_ERROR,
      error
    );
  }
}
