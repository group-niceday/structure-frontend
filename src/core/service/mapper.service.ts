import {ClassConstructor, ClassTransformer, plainToClass} from 'class-transformer';

import {Pageable} from '@/app/shared/model/pageable.model';

export class Mapper extends ClassTransformer {
  toObject<T>(type: ClassConstructor<T>, source: T): T {
    return plainToClass(type, source, {excludeExtraneousValues: true});
  }

  toArray<T>(type: ClassConstructor<T>, source: T[]): T[] {
    return plainToClass(type, source, {excludeExtraneousValues: true});
  }

  toPage<T>(type: ClassConstructor<T>, source: Pageable.Response.Page<T>): Pageable.Response.Page<T> {
    const page = plainToClass(Pageable.Response.Page, source, {excludeExtraneousValues: true}) as Pageable.Response.Page<T>;
    page.content = plainToClass(type, source.content, {excludeExtraneousValues: true}) as T[];
    return page;
  }
}
